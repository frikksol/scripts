#!/usr/bin/env bash

choices=$'laptop\nexternal-display\nexternal-display-gaming'
choice=$(echo "$choices" | dmenu -i -p "What to do?")

external=""
built_in="eDP"

# Since I switch between having HDMI and DP inputs I autocheck for that also
external_is_hdmi=$(xrandr | grep HDMI)
if [ -z "$external_is_hdmi" ]
then
		external="DP"
else
		external="HDMI"
fi

# Since xrandr randomly chooses to have or not to have spaces in display names, we need to check
with_dashes=$(xrandr | grep eDP-1)
echo $with_dashes
if [ -z "$with_dashes" ]
then
		# Dashes not used
		external+=1
		built_in+=1
else
		# Dashes used
		external+=-1
		built_in+=-1
fi

case "$choice" in
		"laptop") xrandr --output $built_in --primary --mode 1920x1080;;
		"external-display") xrandr --output $external --primary --mode 3840x2160 --output $built_in --mode 1920x1080 --right-of $external;;
		"external-display-gaming") xrandr --output $external --primary --mode 1920x1080 --output $built_in --mode 1920x1080 --right-of $external;;
esac

# Restart polybar
$HOME/.config/polybar/launch.sh

# Restart feh for background images
$HOME/.fehbg &

