#!/usr/bin/env bash

function surf_report {
        token=$(cat msw_key)
        wget -q -O temp_file http://magicseaweed.com/api/$token/forecast/?spot_id=$1
        max_wave_size=$(cat temp_file | awk '{gsub(/,/,"\n")}1' | grep maxBreakingHeight | awk '{gsub(/"maxBreakingHeight":/,"")}1' | sort -rg | uniq | head -n 1)

        if [ $max_wave_size -gt 6 ]; then
                echo "There are waves at $2!"
        fi
        rm temp_file
}

function wind_report {
        wget -q -O temp_file https://api.met.no/weatherapi/locationforecast/2.0/compact\?lat=$1\&lon=$2
        result=$(cat temp_file)
	max_wind=$(cat temp_file | awk '{gsub(/,/,"\n")}1' | grep wind_speed | awk '{gsub(/"wind_speed":/,"")}1' | cut -d'.' -f1 | sort -rg | uniq | head -n 1)

        if [ $max_wind -gt 10 ]; then
                echo "There is wind at $3!"
        fi
        rm temp_file
}

surf_report 678 Karmøy
surf_report 1893 Ervik

wind_report 59.3321 10.6670 Larkollen
wind_report 59.0267 10.5245 Færder
wind_report 59.6139 10.4182 Verket

echo "Remeber to log into bibucket"
