#!/usr/bin/env bash

choices=$'suspend\nlock screen\nlog out\nrestart\nshut down'
choice=$(echo "$choices" | dmenu -i -p "What to do?")

case "$choice" in
	"suspend") systemctl suspend;;
	"lock screen") xflock4;;
	"log out") bspc quit 1;;
	"restart") systemctl -i reboot;;
	"shut down") systemctl -i poweroff;;
esac
